--- README  -------------------------------------------------------------

Views Arg Order Sort

Using this module will give you a new sort type with a variety of options where you can choose what field to apply the argument values to for sorting.

--- INSTALLATION and USAGE ----------------------------------------------

1 - use composer require 'drupal/views_arg_order_sort'
2 - create a view and add contextual filter e.g for Content ID
3 - add a basic view sort field 'Multi-item Argument Order'
4 - select 'Content ID' from the select list and configure other settings as you want
5 - pass your ids to the view filter using custom code
6 - you can see nodes in view with your desired order
